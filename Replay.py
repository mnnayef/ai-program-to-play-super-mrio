from nes_py.wrappers import JoypadSpace
import gym_super_mario_bros
from gym_super_mario_bros.actions import RIGHT_ONLY, SIMPLE_MOVEMENT, COMPLEX_MOVEMENT

from AIManagerCopy import reward_function

import sys
import numpy as np
from threading import *
import multiprocessing
from multiprocessing.managers import BaseManager
from tkinter import *
import time
from shutil import copy2
from pathlib import Path
import random
from math import *

import torch
import torch.nn as nn

from smb_cnn import CNN
from smb_ai import Mario
from MarioConfig import * 

from PIL import Image
from pathlib import Path
import glob
import os

######
# 240 x 256, 15 tall (height), 16 wide (width)
# We don't care about the two top blocks (final size is 13x16, [2-15, 0-15])
######

#####################################################
# Setup code for mario vision                       #
#####################################################
drawingTk = Tk()
canvas = Canvas(drawingTk, height=300, width=250)

#####################################################
# Setup code for mario vision                       #
#####################################################
'''
white = bgcolors_images
red = enemies_images
green = blocks_images
yellow = qblocks_images
blue = hazards_images
saddle brown = bgelements_images
gray = vines_images
purple = bowser_images
black = mario_images
gold = pipes_images
orange = flags_images
aquamarine = semiplatforms_images
hot pink = coins_images
cyan = powerups_images
violet = springs_images'''

class_color = ['white', 'red', 'green', 'yellow', 'blue', 'saddle brown', 'gray', 'purple', 'black', 'gold', 'orange', 'aquamarine', 'hot pink', 'cyan', 'violet']
def drawMarioVision(vision, y, x):
    canvas.delete(ALL)

    for i in range(y):
        for j in range(x):
            canvas.create_rectangle(16*i, 16*j, 16*i+16, 16*j+16, fill=class_color[vision[j*y+i]])
    canvas.update()
    canvas.pack()

#####################################################
# Loads the latest trained CNN                      #
#####################################################
def load_cnn():
    cnn = CNN(4)
    cnn_state = torch.load("cnn_training/models/best.pth", map_location=torch.device('cpu'))
    cnn.load_state_dict(cnn_state)
    cnn.eval()
    del cnn_state
    torch.cuda.empty_cache()
    return cnn

#####################################################
# Saved Mario AI instance and the cnn it used       #
# Saved in folder with population and index         #
# So it can be replayed later                       #
#####################################################
def save_agent(mario, index, population, reward):
    print("saving mario")
    directory = Path("ai_training/models/pop{}-ind{}".format(population, index))
    directory.mkdir(parents=True, exist_ok=True)
    torch.save(mario.state_dict(), "ai_training/models/pop{}-ind{}/ind{}-reward{}.pth".format(population, index, index, reward))
    copy2("cnn_training/models/best.pth", "ai_training/models/pop{}-ind{}/cnn.pth".format(population, index))

#####################################################
# Loads Mario AI and it's accompanying CNN          #
# Returns (Mario, CNN)                              #
#####################################################
def load_agent(population, index, reward):
    layers = ((cnn_input,9), (9, 5))
    W = []
    b = []
    Nw = []
    Nb = []
    Ws = []
    Bs = []
    for layer in layers:
        W.append(torch.rand(layer))
        b.append(torch.rand((layer[1])))
    for j in range(len(layers)):
        Ws.append(W[j].float())
        Bs.append(b[j].float())
    directory = Path("ai_training/models/pop{}-ind{}".format(population, index))
    directory.mkdir(parents=True, exist_ok=True)
    string = glob.glob("ai_training/models/pop{}-ind{}/ind{}-*-reward{}.pth".format(population, index, index,reward))
    print(string)
    mario = Mario(Ws, Bs, True)
    mario.load_state_dict(torch.load(string[0]))
    mario.eval()
    cnn = CNN(4)
    cnn.load_state_dict(torch.load("ai_training/models/pop{}-ind{}/cnn.pth".format(population, index), map_location=torch.device('cpu')))
    cnn.eval()
    return mario, cnn


####### MARIO VISION VARIABLES (changes untested) #######
from_x = 6
to_x = 16
from_y = 2
to_y = 15
cnn_input=((to_x-from_x)*(to_y-from_y))

def demo_agent(mario, cnn, R, map_to_use=None, make_movie=False):
    device = torch.device('cpu')
    print("Starting mario {}-{}".format(population,index))

    stage_string = None
    if map_to_use is not None:
        stage_string = map_to_use
    else:
        stage_string = "SuperMarioBros-2-3-v0"


    env = gym_super_mario_bros.make(stage_string)
    env = JoypadSpace(env, RIGHT_ONLY)
    done = False
    state = env.reset()
    frames = []
    total_reward = 0.0
    for episode in range(1):
        ticks_counter = 0
        old_x = 0
        ticks_since_move = 0
        max_x = 0 # For one level
        max_total_x = 0 #For multiple levels
        max_y = 0
        while not done:
            ticks_counter += 1
            
            img = np.array(state)
            blocks = img.reshape(img.shape[0]//16, 16, img.shape[1]//16, 16, 3).swapaxes(1, 2)
            blocks = blocks[from_y:to_y,from_x:to_x,:,:]
            greyscale = np.sum(blocks, axis=4, keepdims=True, dtype='d').reshape(-1,1,16,16)/3

            tensor = torch.tensor(greyscale).reshape(-1,1,16,16).float()
            vision = cnn.f(tensor)
            mario_vision = vision.argmax(1)

            action = mario(mario_vision.reshape(1,-1).float().to(device))

            drawMarioVision(mario_vision, to_x-from_x, to_y-from_y)
            env.render()

            state, reward, done, info = env.step(action.item())

            ##### Change reward #####
            total_reward += reward

            if info['x_pos'] > max_x:
                max_x = info['x_pos']
            if info['flag_get']:
                print("!!!WE GOT TO THE FLAG!!!")
                total_reward += int(5000000)
                max_total_x += max_x
                max_x = 0
            if info['life'] < 2:
                done = True
            if abs(old_x - info['x_pos']) < 1:
                ticks_since_move += 1
            else:
                ticks_since_move = 0
                old_x = info['x_pos']
            if info['y_pos'] > max_y:
                max_y = info['y_pos']
            #exit conditions
            if ticks_since_move > 300:
                done = True
            if done:
                max_total_x += max_x
                fitness = reward_function(max_total_x, max_y, info['score'], ticks_counter)
                total_reward += fitness
            #Statistics
            #Make movie?
            if make_movie:
                frames.append(Image.fromarray(state, 'RGB'))
        state = env.reset()
    env.close()
    print("Mario done, ticks: {}, reward: {}".format(ticks_counter, total_reward))
    if make_movie:
        print("Making movie...")
        directory = Path("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}".format(population, index, population, index, R))
        directory.mkdir(parents=True, exist_ok=True)
        frame_name = 0
        for image in frames:
            image.save("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}/{}.jpg".format(population, index, population, index, R, str(frame_name)))
            frame_name += 1
        frames_count = len(frames)
        import ffmpeg
        (
            ffmpeg
            .input("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}/%d.jpg".
                format(population, index, population, index, R), start_number=0, framerate=50)
            .output("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}/{}.mp4".
                format(population, index, population, index, R, map_to_use), vframes=frames_count)
            .run()
        )
        #clean up images
        globbed = [f for f in glob.glob("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}/*.jpg".format(population, index, population, index, R))]
        for f in globbed:
            os.remove(f)
        

    print("Mario {}-{} complete!".format(population, index))

def NES(npop, learning_rate, sigma, layers, W, b, Nw, Nb, R):
    #print("NES INPUT:")
    #print("npop: ", npop)
    #print("learning rate: ", learning_rate)
    #print("len(W): ", len(W))
    #print("len(b) ", len(b))
    #print("W[0].shape: ", W[0].shape)
    #print("b[0].shape: ", b[0].shape)
    #print("len(R): ", len(R))
    #print("W: ", W)
    #print("b: ", b)
    #print("Nw: ", Nw)
    #print("Nb: ", Nb)
    #print("R: ", R)
    A = None
    Rstd = np.std(R)
    if Rstd == 0:
        A = R-np.mean(R)
    else:
        A = (R - np.mean(R)) / np.std(R)
    for i in range(len(layers)):
        W[i] = W[i] + (learning_rate/(npop*sigma)) * np.dot(Nw[i].transpose(1,2,0), A)
        b[i] = b[i] + (learning_rate/(npop*sigma)) * np.dot(Nb[i].T, A)

def demo(population, index, reward, make_movie, all_stages=False):
    mario, cnn = load_agent(population,index, reward)
    if all_stages is True:
        #print("All stages true")
        for i in range(1, 9): #World
            for j in range(1, 5): #Stage
                stage_string = "SuperMarioBros-"+str(i)+"-"+str(j)+"-v0"
                demo_agent(mario, cnn, reward, stage_string, make_movie)
    else:
        demo_agent(mario, cnn, reward, make_movie=make_movie)

if __name__ == "__main__":
    print(sys.argv)
    population = sys.argv[1]
    index = sys.argv[2]
    reward = sys.argv[3]
    make_movie = sys.argv[4]
    make_movie_all = False
    if make_movie == "yes":
        print("We should make a movie!")
        make_movie = True
    else:
        make_movie = False
    if make_movie_all == "yes":
        make_movie_all = True
    if len(sys.argv) == 6:
        #print("6 args")
        demo(population, index,reward, make_movie, make_movie_all)
    else:
        demo(population, index, reward, make_movie=make_movie)
