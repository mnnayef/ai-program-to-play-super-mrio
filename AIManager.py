from nes_py.wrappers import JoypadSpace
import gym_super_mario_bros
from gym_super_mario_bros.actions import RIGHT_ONLY, SIMPLE_MOVEMENT, COMPLEX_MOVEMENT

import numpy as np
from threading import *
import multiprocessing
from multiprocessing.managers import BaseManager
from tkinter import *
import time
from shutil import copy2
from pathlib import Path
import random
from math import *

import torch
import torch.nn as nn

from smb_cnn import CNN
from smb_ai import Mario

from PIL import Image
from pathlib import Path
import glob
import os

######
# 240 x 256, 15 tall (height), 16 wide (width)
# We don't care about the two top blocks (final size is 13x16, [2-15, 0-15])
######

#####################################################
# Setup code for mario vision                       #
#####################################################
drawingTk = Tk()
canvas = Canvas(drawingTk, height=300, width=250)

#####################################################
# Setup code for mario vision                       #
#####################################################
def drawMarioVision(vision, y, x):
    canvas.delete(ALL)
    '''
    white = bgcolors_images
    red = enemies_images
    green = blocks_images
    yellow = qblocks_images
    blue = hazards_images
    saddle brown = bgelements_images
    gray = vines_images
    purple = bowser_images
    black = mario_images
    gold = pipes_images
    orange = flags_images
    aquamarine = semiplatforms_images
    hot pink = coins_images
    cyan = powerups_images
    violet = springs_images'''

    class_color = ['green', 'white', 'blue', 'red', 'blue', 'saddle brown', 'gray', 'purple', 'black', 'gold', 'orange', 'aquamarine', 'hot pink', 'cyan', 'violet']
    for i in range(y):
        for j in range(x):
            canvas.create_rectangle(16*i, 16*j, 16*i+16, 16*j+16, fill=class_color[vision[j*y+i]])
    canvas.update()
    canvas.pack()
'''
def drawMarioVision(vision):
    canvas.delete(ALL)
    class_color = ['white', 'red', 'green']
    for i in range(16):
        for j in range(13):
            canvas.create_rectangle(16*i, 16*j, 16*i+16, 16*j+16, fill=class_color[vision[j*16+i]])
    canvas.update()
    canvas.pack()
'''

#####################################################
# Loads the latest trained CNN                      #
#####################################################
def load_cnn():
    cnn = CNN(4)
    cnn_state = torch.load("cnn_training/models/best2.pth", map_location=torch.device('cpu'))
    cnn.load_state_dict(cnn_state)
    cnn.eval()
    del cnn_state
    torch.cuda.empty_cache()
    return cnn

#####################################################
# Saved Mario AI instance and the cnn it used       #
# Saved in folder with population and index         #
# So it can be replayed later                       #
#####################################################
def save_agent(mario, index, population, reward, stage_string):
    directory = Path("ai_training/models/pop{}-ind{}".format(population, index))
    directory.mkdir(parents=True, exist_ok=True)
    torch.save(mario.state_dict(), "ai_training/models/pop{}-ind{}/ind{}-{}-reward{}.pth".format(population, index, index, stage_string, reward))
    copy2("cnn_training/models/best2.pth", "ai_training/models/pop{}-ind{}/cnn.pth".format(population, index))

#####################################################
# Loads Mario AI and it's accompanying CNN          #
# Returns (Mario, CNN)                              #
#####################################################
def load_agent(population, index, reward, stage_string):
    layers = ((cnn_input,9), (9, 5))
    W = []
    b = []
    Nw = []
    Nb = []
    Ws = []
    Bs = []
    for layer in layers:
        W.append(torch.rand(layer))
        b.append(torch.rand((layer[1])))
    for j in range(len(layers)):
        Ws.append(W[j].float())
        Bs.append(b[j].float())
    directory = Path("ai_training/models/pop{}-ind{}".format(population, index))
    directory.mkdir(parents=True, exist_ok=True)
    mario = Mario(Ws, Bs, True)
    mario.load_state_dict(torch.load("ai_training/models/pop{}-ind{}/ind{}-{}-reward{}.pth".format(population, index, index, stage_string, reward)))
    mario.eval()
    cnn = CNN(4)
    cnn.load_state_dict(torch.load("ai_training/models/pop{}-ind{}/cnn.pth".format(population, index), map_location=torch.device('cpu')))
    cnn.eval()
    return mario, cnn


####### MARIO VISION VARIABLES (changes untested) #######
from_x = 6
to_x = 16
from_y = 2
to_y = 15
cnn_input=((to_x-from_x)*(to_y-from_y))

def rolling_window(arr, window):
    """Very basic multi dimensional rolling window. window should be the shape of
    of the desired subarrays. Window is either a scalar or a tuple of same size
    as `arr.shape`.
    """
    shape = np.array(arr.shape*2)
    strides = np.array(arr.strides*2)
    window = np.asarray(window)
    shape[arr.ndim:] = window # new dimensions size
    shape[:arr.ndim] -= window - 1
    if np.any(shape < 1):
        raise ValueError('window size is too large')
    return np.lib.stride_tricks.as_strided(arr, shape=shape, strides=strides)

#########################################################
# Agent training, meant to be used with multiprocessing #
# Ws should be a list of Weights for nn.Linear          #
# Bs should be a list of biases for nn.Linear           #
# Ws and Bs used to construct Mario                     #
# index is Marios index in the population               #
# population is the current generation of marios        #
#########################################################
def train_agent(Ws, bs, index, population, R, stage_string, cnn=None):
    device = torch.device('cpu')
    mario = Mario(Ws, bs, True)
    mario.eval()
    mario.to(device)
    print("Starting mario {}-{}".format(population,index))
    if cnn == None:
        cnn = CNN(4)
        cnn_state = torch.load("cnn_training/models/best2.pth")
        cnn.load_state_dict(cnn_state)
        cnn.eval()
        del cnn_state
        torch.cuda.empty_cache()


    env = gym_super_mario_bros.make(stage_string)
    env = JoypadSpace(env, RIGHT_ONLY)
    done = False
    state = env.reset()

    for episode in range(1):
        ticks_counter = 0
        old_x = 0
        ticks_since_move = 0
        max_x = 0 # For one level
        max_total_x = 0 #For multiple levels
        while not done:
            ticks_counter += 1

            img = np.array(state)
            blocks = img.reshape(img.shape[0]//16, 16, img.shape[1]//16, 16, 3).swapaxes(1, 2)
            blocks = blocks[from_y:to_y,from_x:to_x,:,:]
            greyscale = np.sum(blocks, axis=4, keepdims=True, dtype='d').reshape(-1,1,16,16)/3

            tensor = torch.tensor(greyscale).float()
            vision = cnn.f(tensor)
            mario_vision = vision.argmax(1)

            action = mario(mario_vision.reshape(1,-1).float().to(device))
            if sys.argv.__contains__("human"):
                drawMarioVision(mario_vision, to_x-from_x, to_y-from_y)
                env.render()

            state, reward, done, info = env.step(action.item())
            ##### Change reward #####
            R[index] += reward

            if info['x_pos'] > max_x:
                max_x = info['x_pos']
            if info['flag_get']:
                print("!!!WE GOT TO THE FLAG!!!")
                R[index] += int(100000+max_x**1.5)
                max_total_x += max_x
                max_x = 0

            if abs(old_x - info['x_pos']) < 1:
                ticks_since_move += 1
            else:
                ticks_since_move = 0
            old_x = info['x_pos']
            #exit conditions
            if ticks_since_move > 300:
                done = True
            if info['life'] < 2:
                done = True
                R[index] = int(R[index]*min(1, 200/info['time'])) #Lose up to half your points depending on how long you lived
            if done:
                max_total_x += max_x
                R[index] += int(max_total_x**1.5)
            #Statistics
            if (ticks_counter % 500) == 0:
                print("Mario {}-{}, position: {}, reward: {}, ticks: {}, world-stage: {}-{}".format(population, index, info['x_pos'], reward, ticks_counter, info['world'], info['stage']))

        state = env.reset()
    env.close()
    print("Mario {}-{} complete!".format(population, index))

def demo_agent(mario, cnn, R, stage_string, make_movie=False):
    device = torch.device('cpu')
    print("Starting mario {}-{}".format(population,index))

    env = gym_super_mario_bros.make('SuperMarioBros-1-1-v0')
    env = JoypadSpace(env, RIGHT_ONLY)
    done = False
    state = env.reset()
    frames = []
    total_reward = 0.0
    for episode in range(1):
        ticks_counter = 0
        old_x = 0
        ticks_since_move = 0
        max_x = 0 # For one level
        max_total_x = 0 #For multiple levels
        while not done:
            ticks_counter += 1
            
            img = np.array(state)
            blocks = img.reshape(img.shape[0]//16, 16, img.shape[1]//16, 16, 3).swapaxes(1, 2)
            blocks = blocks[from_y:to_y,from_x:to_x,:,:]
            greyscale = np.sum(blocks, axis=4, keepdims=True, dtype='d').reshape(-1,1,16,16)/3

            tensor = torch.tensor(greyscale).reshape(-1,1,16,16).float()
            vision = cnn.f(tensor)
            mario_vision = vision.argmax(1)

            action = mario(mario_vision.reshape(1,-1).float().to(device))
            drawMarioVision(mario_vision, to_x-from_x, to_y-from_y)
            env.render()
            state, reward, done, info = env.step(action.item())
            ##### Change reward #####
            total_reward += reward

            if info['x_pos'] > max_x:
                max_x = info['x_pos']
            if info['flag_get']:
                print("!!!WE GOT TO THE FLAG!!!")
                total_reward += int(100000+max_x**1.5)
                max_total_x += max_x
                max_x = 0

            if abs(old_x - info['x_pos']) < 1:
                ticks_since_move += 1
            else:
                ticks_since_move = 0
            old_x = info['x_pos']
            #exit conditions
            if ticks_since_move > 300:
                done = True
            if info['life'] < 2:
                done = True
                total_reward = int(total_reward*min(1, 200/info['time'])) #Lose up to half your points depending on how long you lived
            if done:
                max_total_x += max_x
                total_reward += int(max_total_x**1.5)
            #Make movie?
            if make_movie:
                frames.append(Image.fromarray(state, 'RGB'))
        state = env.reset()
    env.close()
    print("Mario done, ticks: {}, reward: {}".format(ticks_counter, total_reward))
    if make_movie:
        print("Making movie...")
        directory = Path("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}-stage{}".format(population, index, population, index, R, stage_string))
        directory.mkdir(parents=True, exist_ok=True)
        frame_name = 0
        for image in frames:
            image.save("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}-stage{}/{}.jpg".format(population, index, population, index, R, stage_string,str(frame_name)))
            frame_name += 1
        frames_count = len(frames)
        import ffmpeg
        (
            ffmpeg
            .input("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}-stage{}/%d.jpg".
                format(population, index, population, index, R, stage_string), start_number=0, framerate=50)
            .output("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}-stage{}/movie.mp4".
                format(population, index, population, index, R, stage_string), vframes=frames_count)
            .run()
        )
        #clean up images
        globbed = [f for f in glob.glob("ai_training/models/pop{}-ind{}/movie/pop{}-ind{}-reward{}-stage{}/*.jpg".format(population, index, population, index, R,stage_string))]
        for f in globbed:
            os.remove(f)
        

    print("Mario {}-{} complete!".format(population, index))

def NES(npop, learning_rate, sigma, layers, W, b, Nw, Nb, R):
        A = None
        Rstd = np.std(R)
        if Rstd == 0:
            A = R-np.mean(R)
        else:
            A = (R - np.mean(R)) / np.std(R)
        for i in range(len(layers)):
            W[i] = W[i] + (learning_rate/(npop*sigma)) * np.dot(Nw[i].transpose(1,2,0), A)
            b[i] = b[i] + (learning_rate/(npop*sigma)) * np.dot(Nb[i].T, A)

def train(stage_string):
    ##### Settings #####
    npop = 1 #pop size
    sigma = 0.1 # std noise
    learning_rate = 0.3 #lr
    devices = ['cpu', 'cuda']
    #start W, b
    layers = ((cnn_input,9), (9, 5))
    W = []
    b = []
    for layer in layers:
        W.append(torch.rand(layer))
        b.append(torch.rand((layer[1])))

    #CNN to give mario vision
    cnn = load_cnn()
    cnn.to(torch.device('cpu'))
    for population in range(1000):  
        agents = []
        Nw = []
        Nb = []
        for i in range(len(layers)):
            Nw.append(np.random.randn(npop, layers[i][0], layers[i][1]))
            Nb.append(np.random.randn(npop, layers[i][1]))

        R = multiprocessing.Array('d', npop, lock=False)
        manager = multiprocessing.Manager()
        namespace = manager.Namespace()
        marios = []

        for i in range(npop):
            Ws = []
            Bs = []
            for j in range(len(layers)):
                Ws.append((W[j] + sigma*Nw[j][i]).float())
                Bs.append((b[j] + sigma*Nb[j][i]).float())
            marios.append((Ws, Bs))
        namespace.marios = marios
        for i in range(npop):

            #if i < ceil(npop/10):
             #   random_map=True
            agents.append(multiprocessing.Process(target=train_agent, args=(namespace.marios[i][0], namespace.marios[i][1], i, population, R, stage_string, cnn)))

        #For process approach
        for process in agents:
            process.start()
        for process in agents:
            process.join()

        max_i = -1
        max_r = -10000000
        for i in range(npop):
            if R[i] > max_r:
                max_i = i
                max_r = R[i]
            print("Reward for Mario {}-{}-{}: {}".format(population, i, stage_string, R[i]))
        save_agent(Mario(namespace.marios[max_i][0], namespace.marios[max_i][1], True), max_i, population, R[max_i], stage_string)
        NES(npop, learning_rate, sigma, layers, W, b, Nw, Nb, R)

def demo(population, index, reward, stage_string, make_movie):
    mario, cnn = load_agent(population,index, reward, stage_string)
    demo_agent(mario, cnn, reward, stage_string, make_movie)

if __name__ == "__main__":
    random_map = False
    stage_string = "SuperMarioBros"
    if random_map:
        world = random.randint(1, 8)
        zone = random.randint(1, 4)
        stage_string += "-" + str(world) + "-" + str(zone) + "-v0"
    else:
        stage_string += "-" + "1" + "-" + "1" + "-v0"
    if len(sys.argv) < 2:
        train(stage_string)
    elif len(sys.argv) < 3:
        train(stage_string)
    else:
        print(sys.argv)
        population = sys.argv[1]
        index = sys.argv[2]
        reward = sys.argv[3]
        make_movie = sys.argv[4]
        make_movie_arg = False
        if make_movie == "yes":
            print("We should make a movie!")
            make_movie_arg = True
        demo(population, index, reward, stage_string, make_movie_arg)
