import torch
import torch.nn as nn
import CNNTrainingDataGenerator
import random
import sys
from PIL import Image
from pathlib import Path

def debug_print(*to_print):
    if sys.argv.__contains__("debug"):
        print(to_print)

class CNN(nn.Module):
    def __init__(self, num_cats):
        super(CNN, self).__init__()
        self.num_cats = num_cats
        #Layers
        self.conv = nn.Conv2d(1, 32, kernel_size=3, padding=1)
        self.pool = nn.AvgPool2d(kernel_size=2, stride=2)
        self.conv2 = nn.Conv2d(32, 8, kernel_size=3, padding=1)
        self.conv3 = nn.Conv2d(16, 8, kernel_size=3, padding=1)
        self.conv4 = nn.Conv2d(8, 4, kernel_size=3, padding=1)
        self.conv5 = nn.Conv2d(16, 4, kernel_size=3, padding=1)
        self.conv6 = nn.Conv2d(4, 4, kernel_size=3, padding=1)
        self.conv7 = nn.Conv2d(4, 4, kernel_size=3, padding=1)
        self.conv8 = nn.Conv2d(4, 4, kernel_size=3, padding=1)
        self.dense = nn.Linear(4*16*4, self.num_cats)
        self.relu = nn.ReLU()
        self.dropoutMid = nn.Dropout(p=0.5)
        self.dropoutLow = nn.Dropout(p=0.1)

    def logits(self, x):

        x = self.conv(x)
        x = self.dropoutLow(x)
        x = self.relu(x)
        #x = self.pool(x)
        #x = self.dropoutLow(x)
        x = self.conv2(x)
        x = self.pool(x)
        x = self.dropoutMid(x)
        x = self.relu(x)
        x = self.conv4(x)
        x = self.dropoutLow(x)
        x = self.relu(x)
        # print(x.shape)
        x = self.conv6(x)
        x = self.conv7(x)
        x = self.conv8(x)
        x = self.dense(x.reshape(-1, 4*16*4))
        return x

    # Predictor
    def f(self, x):
        ret = torch.softmax(self.logits(x), dim=1)
        return ret

    # Cross Entropy loss
    def loss(self, x, y):
        return nn.functional.cross_entropy(self.logits(x), y.argmax(1))

    # Accuracy
    def accuracy(self, x, y):
        return torch.mean(torch.eq(self.f(x).argmax(1), y.argmax(1)).float())

def train(cnn):
    x_train, y_train = CNNTrainingDataGenerator.readTrainingData()
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    print("Total datasize: ", len(x_train))

    ####### test split #######
    x_len = len(x_train)
    train = int(len(x_train)*0.8)
    test = x_len - train
    x_test = []
    y_test = []

    for i in range(test):
        #Get a random valid index
        x = random.randint(0, x_len-1)
        #Remove corresponding entry from x_train and y_train
        x_value = x_train.pop(x)
        y_value = y_train.pop(x)
        #Add corresponding entry to x_test and y_test
        x_test.append(x_value)
        y_test.append(y_value)

        #Decrease length by one
        x_len = x_len-1
    ############################################

    x_train = torch.tensor(x_train, device=device).reshape(-1, 1, 16, 16).float()
    y_train_tensor = torch.zeros(len(y_train), cnn.num_cats, device=device)
    for i in range(len(y_train)):
        y_train_tensor[i][y_train[i]] = 1
    y_train = y_train_tensor

    x_test = torch.tensor(x_test, device=device).reshape(-1, 1, 16, 16).float()
    y_test_tensor = torch.zeros(len(y_test), cnn.num_cats, device=device)
    for i in range(len(y_test)):
        y_test_tensor[i][y_test[i]] = 1
    y_test = y_test_tensor

    print("x_train shape: ", x_train.shape)
    print("y_train shape: ", y_train.shape)

    print("x_test shape: ", x_test.shape)
    print("y_test shape: ", y_test.shape)

    batches = 32    #Tweak for speed/accuracy
    x_train_batches = torch.split(x_train, batches)
    y_train_batches = torch.split(y_train, batches)
 

    learning_rate = 0.0001 #Tweak for speed/accuracy
    optimizer = torch.optim.Adam(cnn.parameters(), learning_rate)

    print("Starting training on device: " + str(device))
    best_accuracy = cnn.accuracy(x_test, y_test)
    best = cnn.state_dict()
    for epoch in range(5000):
        for batch in range(len(x_train_batches)):
            cnn.loss(x_train_batches[batch], y_train_batches[batch]).backward()  # Compute loss gradients
            optimizer.step()  # Perform optimization by adjusting W and b,
            optimizer.zero_grad()  # Clear gradients for next step
        accuracy = cnn.accuracy(x_test, y_test)
        if accuracy > best_accuracy:
            best_accuracy = accuracy
            best = cnn.state_dict()
        if (epoch+1)%100 == 0:
            print("Epoch: ", epoch)
            print("best accuracy = %s" % best_accuracy)
            print("best accuracy = %s" % accuracy)
            torch.save(cnn.state_dict(), "cnn_training/models/best2.pth")
    directory = Path('cnn_training/models')
    directory.mkdir(parents=True, exist_ok=True)
    torch.save(best, "cnn_training/models/best2.pth")
    print("best accuracy = %s" % best_accuracy)

    ##### Manual tests #####
    png1 = CNNTrainingDataGenerator.getPixelData(Image.open('size.png'))
    grey_scale = []
    for pixel in png1:
        average = (pixel[0]+pixel[1]+pixel[2])//3
        grey_scale.append(average)
    png1_t = torch.tensor(grey_scale).reshape(1, 1, 32, 32).float().to(device)
    print("png1_t classified as: ", cnn.f(png1_t).argmax(1))

if __name__ == "__main__":
    ##### Check for CUDA availability #####
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    CNNTrainingDataGenerator.generateTrainingData(True)
    cnn = None
    num_cats = 4
    if sys.argv.__contains__("con"):
        print("Contiuing training previous best.")
        cnn = CNN(num_cats)
        cnn.load_state_dict(torch.load("cnn_training/models/best2.pth"))
        cnn.train()
        cnn.to(device)
    else:
        cnn = CNN(num_cats).to(device)
    train(cnn)