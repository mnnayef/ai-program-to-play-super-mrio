import numpy as np

from smb_cnn import CNN
from smb_ai import Mario

'''
    MarioHyperParamters
    Ws is the initial base weights when training
    Bs is the initial base bias when training
    sigma is the standard deviation noise (0.-1.)
    learning_rate is the amount of change from one generation to the next
'''
class MarioHyperparameters():
    def __init__(self, Ws, bs, sigma, learning_rate):
        self.Ws = Ws
        self.bs = bs
        self.sigma = sigma
        self.learning_rate = learning_rate

'''
    MarioConfig
    nn_shapes is a tuple of tuples describing the shape of our neural net ex: ((cnn_output,9), (9, 5))
    nn_activation is a string that is used to index into a map of activation functions for each layer (None for none)
    map_trained_on which map is this batch of marios trained on (can still use ceil(npop/10) for random maps using the same RNG)
    mario_vision shape tuple of tuples ((y_from, y_to), (x_from, x_to)) decides CNN input and output + visualization
    cnn_path relative string path to the cnn to load
    random_seed the seed used by this config. (number or numpy rng state)
    hyperparams instance of MarioHyperParameters describing population, inital weights and learning rate
    nodeid unique ID that describes self's owning worker
'''
class MarioConfig():
    def __init__(self, npop, nn_shapes, nn_activations, 
            map_trained_on, mario_vision_shape, cnn_path, random_state, hyperparameters, 
            nodeid, num_cats, update_function=None):
        self.npop = npop
        self.nn_shapes = nn_shapes
        self.nn_activations = nn_activations
        self.map_trained_on = map_trained_on
        self.mario_vision_shape = mario_vision_shape
        self.cnn_path = cnn_path
        self.random = random_state
        self.hyperparameters = hyperparameters
        self.nodeid = nodeid
        self.update_function = update_function
        self.num_cats = num_cats
        self.population = 0

        self.W = self.hyperparameters.Ws
        self.b = self.hyperparameters.bs

    def generate_agents(self):
        Nw = []
        Nb = []
        Ws = []
        Bs = []
        for i in range(len(self.nn_shapes)):
            Nw.append(self.random.randn(self.npop, self.nn_shapes[i][0], self.nn_shapes[i][1]))
            Nb.append(self.random.randn(self.npop, self.nn_shapes[i][1]))
        for i in range(self.npop):
            Wsn = []
            Bsn = []
            for j in range(len(self.nn_shapes)):
                Wsn.append((self.W[j] + self.hyperparameters.sigma*Nw[j][i]).float())
                Bsn.append((self.b[j] + self.hyperparameters.sigma*Nb[j][i]).float())
            Ws.append(Wsn)
            Bs.append(Bsn)
        return Nw,Nb,Ws,Bs

    def update(self, npop, results, Nw, Nb):
        print("MarioConfig before self.update")
        self.update_function(npop, self.hyperparameters.learning_rate, 
            self.hyperparameters.sigma, self.nn_shapes, self.W, self.b, Nw, Nb, results)
        print("MarioConfig after self.update")
        self.population += 1