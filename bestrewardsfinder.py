from pathlib import Path
import glob
import re

all_paths = []
all_numbers = []
best_high = []
for path in glob.glob("ai_training/models/**/*.pth", recursive=True):
    all_paths.append(path)
for i in range(len(all_paths)):
    scores = re.findall(r"[^a-zA-Z]+\d.pth", all_paths[i])
    if len(scores) == 1:
        score = ''.join(scores[0])
        all_numbers.append((i, float(score[:-4])))
for index, number in all_numbers:
    best_high.append((index,  number))
number_of_entries = 20
best_high = sorted(best_high, reverse=True, key=lambda x: x[1])[0:number_of_entries]

pops = []
inds = []
rewards = []
for index, reward in best_high:
    pop = re.search(r"pop(\d+)", all_paths[index])
    pops.append(pop.group(0)[3:])
    ind = re.search(r"ind(\d+)", all_paths[index])
    inds.append(ind.group(0)[3:])
    rewards.append(reward)

for i in range(len(pops)):
    print(pops[i],"-",inds[i],"-",rewards[i])

#for i in range(len(pops)):
#    os.system("python quicktest.py {} {} {} yes".format(pops[i], inds[i], rewards[i]))
