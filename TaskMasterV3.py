import pickle
import time
import uuid
import numpy as np
import torch
import sys
from copy import *
from shutil import copy2
from pathlib import Path
from math import *

from MarioConfig import *
from NetworkingCommons import *

#####################################################
# Saved Mario AI instance and the cnn it used       #
# Saved in folder with population and index         #
# So it can be replayed later                       #
#####################################################
def save_agent(mario, index, population, reward, stage_string):
    directory = Path("ai_training/models/pop{}-ind{}".format(population, index))
    directory.mkdir(parents=True, exist_ok=True)
    torch.save(mario.state_dict(), "ai_training/models/pop{}-ind{}/ind{}-{}-reward{}.pth".format(population, index, index, stage_string, reward))
    copy2("cnn_training/models/best.pth", "ai_training/models/pop{}-ind{}/cnn.pth".format(population, index))

#####################################################
# Loads Mario AI and it's accompanying CNN          #
# Returns (Mario, CNN)                              #
#####################################################
def load_agent(population, index, reward, stage_string, cnn_output):
    layers = ((cnn_output,9), (9, 5))
    W = []
    b = []
    Nw = []
    Nb = []
    Ws = []
    Bs = []
    for layer in layers:
        W.append(torch.rand(layer))
        b.append(torch.rand((layer[1])))
    for j in range(len(layers)):
        Ws.append(W[j].float())
        Bs.append(b[j].float())
    directory = Path("ai_training/models/pop{}-ind{}".format(population, index))
    directory.mkdir(parents=True, exist_ok=True)
    mario = Mario(Ws, Bs, True)
    mario.load_state_dict(torch.load("ai_training/models/pop{}-ind{}/ind{}-{}-reward{}.pth".format(population, index, index, stage_string, reward)))
    mario.eval()
    cnn = CNN(4)
    cnn.load_state_dict(torch.load("ai_training/models/pop{}-ind{}/cnn.pth".format(population, index), map_location=torch.device('cpu')))
    cnn.eval()
    return mario, cnn

class Server(Thread):
    def __init__(self):
        super(Server, self).__init__()
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #IPv4 TCP
        self.server.bind(('', 15025)) #Listen to all interfaces, port 15025
        self.connection_lock = Lock()
        self.clients = []
        self.done = False

    def run(self):
        try:
            self.server.listen(3)
            while not self.done:
                (client, address) = self.server.accept() # Provided workers have static IP we can use a whitelist
                self.connection_lock.acquire()
                self.clients.append((client,address))
                self.connection_lock.release()
        except:
            print("wtf")
        

class TaskMasterGlobals():
    def __init__(self):
        #############################
        # TaskMaster global variables
        #############################

        self.sockets        = [] #sockets (clients)
        self.addresses      = {} #Map of (ip, port) indexed by client socket
        self.in_buffers     = {} #Map of bytes indexed by client socket
        self.out_buffers    = {} #Map of bytes indexed by client socket
        self.configs        = {} #Map of MarioConfig indexed by client socket
        self.nodeids        = {}
        self.rewards        = {}
        self.ready          = {} #Map of bool indexed by client socket
        self.training       = {} #Map of bool indexed by client socket

        self.is_training = False

        self.globalrandom = np.random.RandomState()
        self.population = 0
        self.npop_total = 0
        self.sigma = 0.2 # std noise
        self.learning_rate = 0.05
        self.from_x = 6
        self.to_x = 16
        self.from_y = 2
        self.to_y = 15
        self.cnn_shape=((self.to_x-self.from_x)*(self.to_y-self.from_y))
        self.mario_vision = ((self.from_y, self.to_y), (self.from_x, self.to_x))
        self.layers = ((self.cnn_shape,9), (9, 5))

        self.old_max = -1.0

        self.W = []
        self.b = []
        for layer in self.layers:
            self.W.append(torch.tensor(self.globalrandom.randn(layer[0], layer[1])))
            self.b.append(torch.tensor(self.globalrandom.randn(layer[1])))
        self.hyperparameters = MarioHyperparameters(self.W, self.b, self.sigma, self.learning_rate)

        self.reset_pop_values()
        # per population variables #
    
    def load_previous_agent(self, base_weights, reward):
        if base_weights is not None:
            self.old_max = float(reward)
            self.hyperparameters = MarioHyperparameters(base_weights[0], base_weights[1], self.sigma, self.learning_rate)

    def reset_pop_values(self):
        self.R   = []
        self.stages   = []
        self.rewards = []
        self.nodeids = []
        self.Ws  = []
        self.Bs  = []
        for i in range(len(self.layers)):
            self.Ws.append(np.empty((0, self.layers[i][0], self.layers[i][1])))
            self.Bs.append(np.empty((0, self.layers[i][1])))
        self.marioWs = []
        self.marioBs = []




#dummy idea to sync the configs... Just "send" the package they get to ourselves
def do_update(packet):
    update_packet = pickle.loads(bytearray(packet))
    rewards = update_packet.rewards
    nodeids = update_packet.nodeids
    population = update_packet.population
    npop_total = update_packet.npop_total
    ##print("nodeids from packet: ", nodeids)
    ##print("do_update population: ", tm_globals.population)
    R = []
    S = []
    Nws = []
    Nbs = []
    for i in range(len(tm_globals.layers)):
        Nws.append(np.empty((0, tm_globals.layers[i][0], tm_globals.layers[i][1])))
        Nbs.append(np.empty((0, tm_globals.layers[i][1])))
    for nodeid in nodeids:
        for _, config in tm_globals.configs.items():
            if nodeid == config.nodeid:
                ##print("generating: ", nodeid)
                Nw, Nb, marioWs, marioBs = config.generate_agents()
                tm_globals.marioWs.extend(marioWs) #Used to save best mario
                tm_globals.marioBs.extend(marioBs) #Used to save best mario
                for i in range(len(tm_globals.layers)):
                    Nws[i] = np.concatenate((Nws[i], Nw[i]), 0)
                    Nbs[i] = np.concatenate((Nbs[i], Nb[i]), 0)
                break
    for rewards in rewards:
        R.extend(rewards)
    for stages in tm_globals.stages:
        S.extend(stages)

    utility_upgrade_configs(tm_globals.configs.values(), npop_total, R, Nws, Nbs)
    max_i = -1
    max_r = -10000000.0
    for i in range(len(R)):
        if R[i] > max_r:
            max_i = i
            max_r = R[i]
    if len(tm_globals.sockets) > 0 and any(tm_globals.training):
        m = Mario(tm_globals.marioWs[max_i], tm_globals.marioBs[max_i], True)
        if max_r > tm_globals.old_max:
            print("New max reward: ", max_r)
            tm_globals.old_max = max_r
            new_Ws, new_bs = m.get_layers()
            tm_globals.hyperparameters.Ws = new_Ws
            tm_globals.hyperparameters.bs = new_bs
        save_agent(m, max_i, tm_globals.population, R[max_i], S[max_i])
        filename = "ai_training/models/pop{}-ind{}".format(tm_globals.population, max_i)
        directory = Path(filename)
        directory.mkdir(parents=True, exist_ok=True)
        with open(filename+"/rewards_for_this_pop.pickle", 'wb') as f:
            pickle.dump(R, f)

# Get CPU cores
# Write Mario Config for this client, distribute to all clients
# Write Mario Config for all other clients to this client
def handle_client_cpu_count(client, length):
    ##print("handle_client_cpu_count start")
    data = bytearray(tm_globals.in_buffers[client][5:length+5]) #strip packet id + message len (1 byte + 4 bytes), get length for message
    del tm_globals.in_buffers[client][0:length+5]
    client_cpu_cores = pickle.loads(data)
    random_state = np.random.RandomState(tm_globals.globalrandom.randint(low = 0, high=2147483645))

    number_agents_supported = int(client_cpu_cores.cpu_cores/2)

    config = MarioConfig(number_agents_supported, tm_globals.layers, None, "SuperMarioBros-v0"
        , tm_globals.mario_vision, "cnn_training/models/best.pth", random_state, deepcopy(tm_globals.hyperparameters), uuid.uuid1(), 4, NES)
    tm_globals.configs[client] = config
    ##print("Nodeidfindme create for: ", config.nodeid)
    ##print("Wfindme create: ", tm_globals.configs[client].W)
    ##print("bfindme create", tm_globals.configs[client].b)
    ##print("config create random state:\n", tm_globals.configs[client].random.get_state())

    response = pickle.dumps(config)
    length = len(response).to_bytes(4, byteorder='little')
    packet_id = server_client_packets["server_config_self"].to_bytes(1, byteorder='little')
    out_packet = packet_id + length + response
    tm_globals.out_buffers[client].append(out_packet)

    send_server_config_others(client)

    ##print("Client CPU cores: ", client_cpu_cores.cpu_cores, " ", tm_globals.addresses[client])
    #print("handle_client_cpu_count end")

# Confirmation that nothing went wrong in the config stage. (Correct node ids returned)
def handle_client_ready(client, length):
    #print("handle_client_ready start")
    data = bytearray(tm_globals.in_buffers[client][5:length+5]) 
    del tm_globals.in_buffers[client][0:length+5]
    client_ready_packet = pickle.loads(data)
    client_nodeids = client_ready_packet.nodeids
    has_all_nodeids = True
    for nodeid in client_nodeids:
        found = False
        for config in tm_globals.configs.values():
            if config is not None and nodeid == config.nodeid:
                found = True
                break
        if found is False:
            has_all_nodeids = False
            break
    if len(client_nodeids) < len(tm_globals.configs):
        has_all_nodeids = False
    if tm_globals.training[client] is False and tm_globals.is_training is False:
        ##print("Received ready from {} with nodeids: {}\n we have nodeids: {}".format(tm_globals.addresses[client], client_nodeids, tm_globals.configs.values()))
        if has_all_nodeids is True:
            ##print("Setting client {} ready".format(tm_globals.addresses[client]))
            tm_globals.ready[client] = True
    if has_all_nodeids is False:
        send_server_config_others(client)
    ##print("handle_client_ready end")

#Distribute rewards after task completion
def handle_client_complete(client, length):
    #print("handle_client_complete start")
    data = bytearray(tm_globals.in_buffers[client][5:length+5]) 
    del tm_globals.in_buffers[client][0:length+5]
    rewards = pickle.loads(data)

    tm_globals.training[client] = False
    ##print("Len rewards.rewards: ", len(rewards.rewards))
    tm_globals.rewards.append(rewards.rewards)
    tm_globals.stages.append(rewards.stages)
    ##print("Len rewards in client_complete")
    tm_globals.nodeids.append(tm_globals.configs[client].nodeid)
    
    #print("handle_client_complete end")

def handle_client_hello(client, length):
    #print("handle_client_hello start")
    del tm_globals.in_buffers[client][0:length+5]
    send_server_hello(client)
    #print("handle_client_hello end")

def update_configs_save_best():
    #print("sending server update config")
    #Check for disconnects, new packages
    do_network_stuff()
    check_inbuffers()
    packet_id = server_client_packets["server_update_config"].to_bytes(1, byteorder='little')
    payload = pickle.dumps(ServerClientUpdateConfig(tm_globals.rewards, tm_globals.nodeids, tm_globals.population, tm_globals.npop_total)) #tm_globals.configs[s].W, tm_globals.configs[s].b, tm_globals.population
    length = len(payload).to_bytes(4, byteorder='little')
    for s in tm_globals.sockets:
        tm_globals.out_buffers[s].append(packet_id+length+payload)
    #print("update_configs_save_best start")
    do_update(payload)
    #Save the best agent before we update

    tm_globals.reset_pop_values()
    tm_globals.population += 1
    tm_globals.is_training = False
    check_for_new_clients()
    #print("update_configs_save_best end")

def send_server_hello(client):
    #print("send_server_hello start")
    packet_id = server_client_packets["server_hello"].to_bytes(1, byteorder='little') # Add server_hello packet header
    packet_pad = (0).to_bytes(4, byteorder='little')
    tm_globals.out_buffers[client].append(packet_id+packet_pad)
    #print("send_server_hello end")


def send_server_client_step():
    packet_id = server_client_packets["server_step"].to_bytes(1, byteorder='little')
    packet_pad = (0).to_bytes(4, byteorder='little')
    npop_total = 0
    all_ready = True
    for c in tm_globals.sockets:
        if not tm_globals.ready[c]:
           all_ready = False
           break
    if all_ready:
        for c in tm_globals.sockets:
            #print("sending step")
            tm_globals.out_buffers[c].append(packet_id+packet_pad)
            npop_total += tm_globals.configs[c].npop
            tm_globals.ready[c] = False
            tm_globals.training[c] = True
        tm_globals.npop_total = npop_total
        tm_globals.is_training = True
    else:
        tm_globals.npop_total = 0

def send_server_config_others(s_self):
    #print("send_server_config_others start")
    configs = []
    for s in tm_globals.sockets:
        if tm_globals.configs[s] is not None and s != s_self:
            configs.append(tm_globals.configs[s])
    packet_id = server_client_packets["server_config_others"].to_bytes(1, byteorder='little')
    payload = pickle.dumps(ServerClientConfigOthers(configs))
    length = len(payload).to_bytes(4, byteorder='little')
    tm_globals.out_buffers[s_self].append(packet_id+length+payload)
    #print("send_server_config_others end")

def extract_packet(client):
    message_length = int.from_bytes(tm_globals.in_buffers[client][1:5], byteorder='little')
    #Make sure client in_buffer has any data at all
    if len(tm_globals.in_buffers[client]) >= (message_length+5):
        packet_id = tm_globals.in_buffers[client][0]
        #print("Packet_id {}, {}".format(packet_id, tm_globals.addresses[client]))
        if packet_id == client_server_packets["client_hello"]:
            handle_client_hello(client, message_length)
        if packet_id == client_server_packets["client_cpu_cores"]:
            handle_client_cpu_count(client, message_length)
        if packet_id == client_server_packets["client_ready"]:
            handle_client_ready(client, message_length)
        if packet_id == client_server_packets["client_complete"]:
            handle_client_complete(client, message_length)
        if packet_id == client_server_packets["client_goodbye"]:
             pass

def disconnect_client(socket):
    try:
        #print("Client disconnected: ", socket)
        #print("Length of configs pre delete: ", len(tm_globals.configs))
        dc_config = tm_globals.configs[socket]
        #Inform still connected clients
        packet_id = server_client_packets["server_client_dc"].to_bytes(1, byteorder='little')
        payload = pickle.dumps(ServerClientDCPacket(dc_config.nodeid))
        length = len(payload).to_bytes(4, byteorder='little')
        for s in tm_globals.sockets:
            #print("Sending dc messages")
            tm_globals.out_buffers[s].append(packet_id+length+payload)
        #Cleanup client resources
        if dc_config.nodeid in tm_globals.nodeids:
            index = -1
            for i in range(len(tm_globals.nodeids)):
                if dc_config.nodeid == tm_globals.nodeids[i]:
                    index = i
                    break
            del tm_globals.rewards[index]
            tm_globals.nodeids.remove(dc_config.nodeid)
            tm_globals.npop_total -= dc_config.npop
        tm_globals.sockets.remove(socket)
        del tm_globals.addresses[socket]
        del tm_globals.configs[socket]
        del tm_globals.in_buffers[socket]
        del tm_globals.out_buffers[socket]
        del tm_globals.ready[socket]

        #print("Length of configs post delete: ", len(tm_globals.configs))
    except Exception as ex:
        print("ERROR: ", ex)


def do_network_stuff():
    if len(tm_globals.sockets) > 0:
        try:
            (read, write, err) = select.select(tm_globals.sockets, tm_globals.sockets, tm_globals.sockets)
            for e in err:
                #print("Oopsie from err for: {}".format(tm_globals.addresses[e]))
                disconnect_client(e)
                return
            for w in write:
                total_sent = 0
                print_after = False
                if len(tm_globals.out_buffers[w]) > 0:
                    print_after = True
                    ##print("Len outbuffers before send: ", len(tm_globals.out_buffers[w]))
                while len(tm_globals.out_buffers[w]) > 0 and total_sent < 4096:
                    if (total_sent + len(tm_globals.out_buffers[w][0]) < 4096) or total_sent == 0:
                        b = tm_globals.out_buffers[w].pop(0)
                        sent = w.send(b)
                        ##print("sent data: ", b)
                        if sent == 0:
                            #print("Oopsie from write for: {}".format(tm_globals.addresses[w]))
                            disconnect_client(w)
                            return
                        else:
                            total_sent += sent
                #if print_after is True:
                    ##print("Len outbuffers after send: ", len(tm_globals.out_buffers[w]))
            for r in read:
                data = []
                data = r.recv(1024)
                if len(data) == 0 or data == b'':
                    #print("Oopsie from read for: {}".format(tm_globals.addresses[r]))
                    disconnect_client(r)
                    return
                else:
                    tm_globals.in_buffers[r].extend(data)
        except Exception as ex:
            print(ex)
            disconnect_client(ex)
            #quit("Exception occured, quitting")
            #server.done = True
def check_inbuffers():
    for s in tm_globals.sockets:
        if len(tm_globals.in_buffers[s]) >= 5:
            extract_packet(s)
def check_for_new_clients():
    server.connection_lock.acquire()
    while len(server.clients) > 0 and tm_globals.is_training is False:
            socket, address = server.clients.pop()
            #print("New connection: {}".format(address))
            tm_globals.sockets.append(socket)
            tm_globals.addresses[socket] = address
            tm_globals.configs[socket] = None
            tm_globals.in_buffers[socket] = []
            tm_globals.out_buffers[socket] = []
            tm_globals.ready[socket] = False    #ready when received configs
            tm_globals.training[socket] = False 
    server.connection_lock.release()
def task_manager_main():
    #Check inbox for new sockets
    if tm_globals.is_training is False:
        check_for_new_clients()
    if len(tm_globals.sockets) > 0:
        #Check in and out buffer and handle packets
        do_network_stuff()
        check_inbuffers()
        #if all clients are ready, and none are running, step
        no_one_training = True
        for s in tm_globals.sockets:
            if tm_globals.training[s]:
                no_one_training = False
                break
        if no_one_training and tm_globals.is_training:
            update_configs_save_best()
        all_ready = True
        for s in tm_globals.sockets:
            if not tm_globals.ready[s]:
                all_ready = False
                break
        if all_ready and no_one_training and not tm_globals.is_training:
            send_server_client_step()


if __name__ == "__main__":
    global tm_globals
    tm_globals = TaskMasterGlobals()
    if len(sys.argv) > 1:
        pop = sys.argv[1]
        ind = sys.argv[2]
        reward = sys.argv[3]
        stage = sys.argv[4]
        mario, _ = load_agent(pop, ind, reward, stage, tm_globals.cnn_shape)
        Ws, bs = mario.get_layers()
        base_weights = (Ws, bs)
        tm_globals.load_previous_agent(base_weights, reward)
        print("Starting at pop-ind with reward {}-{}-{}".format(pop,ind,reward))

    global server
    server = Server()
    server.daemon = True
    print("Evil TaskMaster has started...")
    server.start()
    ticks = 0
    while server.is_alive():
        ticks += 1
        task_manager_main()
        if ticks == 500000:
            ticks = 0
            print("Connected client ready states: Training: {} with {} workers, pop: {}".format(tm_globals.is_training, tm_globals.npop_total, tm_globals.population))
            for s in tm_globals.sockets:
                print("Worker {}: Ready: {}, Training: {}".format(tm_globals.addresses[s] ,tm_globals.ready[s], tm_globals.training[s]))
    print("Evil TaskMaster has stopped...")