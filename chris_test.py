from PIL import Image, ImageOps, ImageDraw
from pathlib import Path
import glob

def shift_x(image, x):
    """Shift an image horizontal."""
    return image.transform(image.size, Image.AFFINE, (1, 0, x, 0, 1, 0))

def shift_y(image, y):
    """Shift an image vertical."""
    return image.transform(image.size, Image.AFFINE, (1, 0, 0, 0, 1, y))

def generateShifts(image, shift=1):
    shiftedImages = []
    for straight in range(-shift, shift+1, 1):
        cp = image
        cp = shift_x(cp, straight)
        if cp.getextrema() != (0,0):
            shiftedImages.append(cp)
        else:
            print("all black image found")
        cp2 = image
        cp2 = shift_y(cp2, straight)
        if cp2.getextrema() != (0,0):
            shiftedImages.append(cp2)
        else:
            print("all black image found")
        for diag in range(-shift, shift+1, 1):
            cp = image
            cp = shift_x(shift_y(cp, diag), straight)
            if cp.getextrema() != (0,0):
                shiftedImages.append(cp)
            else:
                print("all black image found")
    return shiftedImages

def merge(foreground, background_images):
    merged_images = []
    for bg_path in background_images:
        bg = Image.open(bg_path)
        fg_cp = foreground.copy()
        bg.paste(fg_cp, (0, 0), fg_cp)
        merged_images.append(bg)
    return merged_images

def roll(im, delta, blocks):
    images = []
    for block in blocks:
        image = im.copy()
        xsize, ysize = image.size
        delta = delta % xsize
        if delta == 0: images.append(image)
        else:
            b = Image.open(block)
            part1 = image.crop((0, 0, delta, ysize))
            part2 = b.crop((delta, 0, xsize, ysize))
            image.paste(part2, (0, 0, xsize-delta, ysize))
            image.paste(part1, (xsize-delta, 0, xsize, ysize))
            images.append(image)
    return images

def alpha_from_right(im, delta):
    image = im.copy()
    xsize, ysize = image.size
    delta = delta % xsize
    if delta == 0: return image

    part1 = image.crop((0, 0, delta, ysize))
    part2 = image.crop((delta, 0, xsize, ysize))
    part2.putalpha(0)
    image.paste(part2, (0, 0, xsize-delta, ysize))
    image.paste(part1, (xsize-delta, 0, xsize, ysize))


    return image

categories = []
for path in glob.glob("cnn_training/data/*"):
    categories.append([f for f in glob.glob(path+"/*.png")])
    print(path.split("/")[len(path.split("/"))-1])

all_images = []
all_images_pixelated = []

container = []
bg_unique = []
blocky_blocks = []

for cat in categories:
    if("bgcolors" in cat[0]):
        cat.sort()
        print("hwhwhw")
        bg_unique.append(cat[0])
        bg_unique.append(cat[16])
        bg_unique.append(cat[29])
        bg_unique.append(cat[36])
        bg_unique.append(cat[44])
        bg_unique.extend(cat[48:99])
    if("blocks" in cat[0]):
        print("ye")
        for pic in cat:
            if("flag" not in pic and "spring" not in pic):
                blocky_blocks.append(pic)


img = Image.open(blocky_blocks[len(blocky_blocks)-9])
img.show()
rolled = roll(img, 10, blocky_blocks)
rolled[28].show()