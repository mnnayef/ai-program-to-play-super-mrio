import sys
import math
import pickle
import time
import uuid
import numpy as np
import torch
import socket
import select
import psutil
import multiprocessing
import ctypes

from nes_py.wrappers import JoypadSpace
import gym_super_mario_bros
from gym_super_mario_bros.actions import RIGHT_ONLY, SIMPLE_MOVEMENT, COMPLEX_MOVEMENT

from MarioConfig import *
from NetworkingCommons import *

#########################################################
# Agent training, meant to be used with multiprocessing #
# Ws should be a list of Weights for nn.Linear          #
# Bs should be a list of biases for nn.Linear           #
# Ws and Bs used to construct Mario                     #
# index is Marios index in the population               #
# population is the current generation of marios        #
#########################################################
def train_agent(Ws, bs, index, population, R, mario_vision, stage=False, cnn=None):
    device = torch.device('cpu')
    mario = Mario(Ws, bs, True)
    mario.eval()
    mario.to(device)
    ##print("Starting mario {}-{}".format(population,index))

    env = gym_super_mario_bros.make(stage)
    env = JoypadSpace(env, RIGHT_ONLY)
    done = False
    state = env.reset()
    #FIXME
    from_y  = mario_vision[0][0]
    to_y    = mario_vision[0][1]
    from_x  = mario_vision[1][0]
    to_x  = mario_vision[1][1]
    for episode in range(1):
        ticks_counter = 0
        old_x = 0
        ticks_since_move = 0
        max_x = 0 # For one level
        max_total_x = 0 #For multiple levels
        max_y = 0
        while not done:
            ticks_counter += 1

            img = np.array(state)
            blocks = img.reshape(img.shape[0]//16, 16, img.shape[1]//16, 16, 3).swapaxes(1, 2)
            blocks = blocks[from_y:to_y,from_x:to_x,:,:]
            greyscale = np.sum(blocks, axis=4, keepdims=True, dtype='d').reshape(-1,1,16,16)/3

            tensor = torch.tensor(greyscale).float()
            vision = cnn.f(tensor)
            mario_vision = vision.argmax(1)

            action = mario(mario_vision.reshape(1,-1).float().to(device))

            state, reward, done, info = env.step(action.item())
            ##### Change reward #####
            R[index] += reward

            if info['x_pos'] > max_x:
                max_x = info['x_pos']
            if info['flag_get']:
                print("!!!WE GOT TO THE FLAG!!!")
                R[index] += int(5000000)
                max_total_x += max_x
                max_x = 0
            if info['life'] < 2:
                done = True
            if abs(old_x - info['x_pos']) < 1:
                ticks_since_move += 1
            else:
                ticks_since_move = 0
                old_x = info['x_pos']
            if info['y_pos'] > max_y:
                max_y = info['y_pos']
            #exit conditions
            if ticks_since_move > 300:
                done = True
            if done:
                max_total_x += max_x
                fitness = reward_function(max_total_x, max_y, info['score'], ticks_counter)
                R[index] += fitness
            #Statistics
            if (ticks_counter % 500) == 0:
                ##print("Mario {}-{}, position: {}, reward: {}, ticks: {}, world-stage: {}-{}".format(population, index, info['x_pos'], reward, ticks_counter, info['world'], info['stage']))
                pass
        state = env.reset()
    env.close()
    ##print("Mario {}-{} complete!".format(population, index))

class Worker():
    def __init__(self):
        np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
        self.socket = None
        self.in_buffer = []
        self.out_buffer = []
        self.config_self : MarioConfig = None
        self.configs = []
        self.configs_nodeids = []
        self.done = False
        self.connected = False
        #Reset all of these per run
        self.nodeids = []
        self.rewards = None
        self.Nw = []
        self.Nb = []
        self.ownNw = []
        self.ownNb = []
        self.R = []
        self.npop_total = 0

    def reset_on_step(self):
        self.nodeids = []
        self.rewards = None
        self.Nw = []
        self.Nb = []
        self.ownNw = []
        self.ownNb = []
        self.R = []
        self.npop_total = 0
        for i in range(len(self.config_self.nn_shapes)):
            self.Nw.append(np.empty((0, self.config_self.nn_shapes[i][0], self.config_self.nn_shapes[i][1])))
            self.Nb.append(np.empty((0, self.config_self.nn_shapes[i][1])))
    
    def train(self):
        cnn = CNN(self.config_self.num_cats)
        cnn_state = torch.load(self.config_self.cnn_path, map_location=torch.device('cpu'))
        cnn.load_state_dict(cnn_state)
        cnn.eval()
        ##print("Wfindme: ", self.config_self.W)
        ##print("bfindme: ", self.config_self.b)
        Nw, Nb, Ws, Bs = self.config_self.generate_agents()
        self.ownNw = Nw
        self.ownNb = Nb
        #print("Generated at population {}".format(self.config_self.population))
        ##print("Nw:\n", Nw)
        ##print("Nb:\n", Nb)
        ##print("Ws:\n", Ws)
        ##print("Bs:\n", Bs)
        agents = []
        R = multiprocessing.Array('d', self.config_self.npop, lock=False)
        S = []

        for i in range(self.config_self.npop):
            random_map = False
            #if i < max(0, math.floor(self.config_self.npop/3)):
            #    random_map=True
            stage_string = "SuperMarioBros"
            if random_map:
                world = np.random.randint(1,8)
                zone = np.random.randint(1,4)
                stage_string += "-"+str(world)+"-"+str(zone)+"-v0"
            else:
                stage_string += "-2-3-v0"
            S.append(stage_string)

            agents.append(multiprocessing.Process(target=train_agent, args=(Ws[i], Bs[i], i, self.config_self.population, R, self.config_self.mario_vision_shape, stage_string, cnn)))

        #For process approach
        for process in agents:
            process.start()
        for process in agents:
            process.join()

        max_i = -1
        max_r = -10000000
        for i in range(self.config_self.npop):
            if R[i] > max_r:
                max_i = i
                max_r = R[i]
            ##print("Reward for Mario {}-{}: {}".format(self.config_self.population, i, R[i]))
        #save_agent(Mario(Ws[max_i], Bs[max_i], True), max_i, population, R[max_i])
        self.R.extend(R)
        #Check for missed messages during training
        self.do_network_stuff()
        self.received_callback()
        self.send_client_complete(R, S)
        self.R = []
        ##print("Train() complete")

    def extract_packet(self):
        packet_id = self.in_buffer[0]
        message_length = int.from_bytes(self.in_buffer[1:4], byteorder='little')
        if len(self.in_buffer) >= (message_length+5):
            #print("packet_id: ", packet_id)
            if packet_id == server_client_packets["server_hello"]:
                self.handle_server_hello(message_length)
            elif packet_id == server_client_packets["server_step"]:
                self.handle_server_step(message_length)
            elif packet_id == server_client_packets["server_config_others"]:
                self.handle_server_config_others(message_length)
            elif packet_id == server_client_packets["server_config_self"]:
                self.handle_server_config_self(message_length)
            elif packet_id == server_client_packets["server_client_dc"]:
                self.handle_server_client_dc(message_length)
            elif packet_id == server_client_packets["server_update_config"]:
                self.handle_server_update_config(message_length)

    def handle_server_client_dc(self, length):
        #print("handle_server_client_dc start")
        data = self.in_buffer[5:length+5]
        del self.in_buffer[0:length+5]
        dc_packet = pickle.loads(bytearray(data))
        self.configs_nodeids.remove(dc_packet.nodeid)
        index_of_config = -1
        for i in range(len(self.configs)):
            if dc_packet.nodeid == self.configs[i].nodeid:
                index_of_config = i
                break
        if index_of_config != -1:
            del self.configs[index_of_config]
        else:
            print("ERROR: Tried deleting non-existent config")
        #print("handle_server_client_dc_ end")

    def received_callback(self):
        if len(self.in_buffer) >= 5:
            self.extract_packet()

    def do_update(self):
        #print("nodeids from packet: ", self.nodeids)
        #print("do_update population: ", self.config_self.population)
        for nodeid in self.nodeids:
            for config in self.configs:
                if nodeid == config.nodeid:
                    if nodeid == self.config_self.nodeid:
                        #print("generating self: ", nodeid)
                        for i in range(len(self.config_self.nn_shapes)):
                            self.Nw[i] = np.concatenate((self.Nw[i], self.ownNw[i]), 0)
                            self.Nb[i] = np.concatenate((self.Nb[i], self.ownNb[i]), 0)
                        break
                    else:
                        #print("generating other: ", nodeid)
                        Nw, Nb, _, _ = config.generate_agents()
                        for i in range(len(self.config_self.nn_shapes)):
                            self.Nw[i] = np.concatenate((self.Nw[i], Nw[i]), 0)
                            self.Nb[i] = np.concatenate((self.Nb[i], Nb[i]), 0)
                        break
        self.R = []
        for rewards in self.rewards:
            self.R.extend(rewards)
        utility_upgrade_configs(self.configs, self.npop_total, self.R, self.Nw, self.Nb)
        self.send_client_ready(True)

    def handle_server_update_config(self, length):
        #print("handle_server_update_config start")
        data = self.in_buffer[5:length+5]
        del self.in_buffer[0:length+5]
        update_packet = pickle.loads(bytearray(data))
        self.rewards = update_packet.rewards
        self.nodeids = update_packet.nodeids
        self.config_self.population = update_packet.population
        self.npop_total = update_packet.npop_total
        self.do_update()
        #print("handle_server_update_config end")

    def handle_server_config_others(self, length):
        #print("handle_server_config_others start")
        data = self.in_buffer[5:length+5]
        del self.in_buffer[0:length+5]
        try:
            config_others = pickle.loads(bytearray(data))
            for config in config_others.configs:
                if self.config_self is None:
                    #print("Haven't received config_self yet")
                    continue
                if config.nodeid == self.config_self.nodeid:
                    continue
                is_new = True
                for existing_config_id in self.configs_nodeids:
                    #print("checking for existing nodeid")
                    if existing_config_id == config.nodeid:
                        is_new = False
                        break
                if is_new:
                    #print("New config received")
                    self.configs.append(config)
                    self.configs_nodeids.append(config.nodeid)
                else: #Might receive a none
                    continue
            self.send_client_ready(True)
            #print("handle_server_config_others end")
        except Exception as ex:
            print("ERROR: ", ex)

                        
    def handle_server_hello(self, length):
        #print("handle_server_hello start")
        data = self.in_buffer[5:length+5]
        del self.in_buffer[0:length+5]
        self.send_client_cpu_cores()
        #print("handle_server_hello end")

    def handle_server_config_self(self, length):
        #print("handle_server_config_self start")
        data = self.in_buffer[5:length+5]
        del self.in_buffer[0:length+5]
        self.config_self = pickle.loads(bytearray(data))
        self.configs_nodeids.append(self.config_self.nodeid)
        self.configs.append(self.config_self)
        self.reset_on_step()
        self.send_client_ready(True)
        #print("Received self config from server. my id: ", self.config_self.nodeid)
        #print("handle_server_config_self end")

    def handle_server_step(self, length):
        #print("handle_server_step start")
        data = self.in_buffer[5:length+5]
        del self.in_buffer[0:length+5]
        self.reset_on_step()
        self.train()
        #print("handle_server_step end")


    def send_client_complete(self, R, S):
        #print("send_client_complete start")
        packet_id = client_server_packets["client_complete"].to_bytes(1, byteorder='little')
        picklable_R = []
        ##print("FINDME R: ", len(R))
        for r in R:
            picklable_R.append(r)
        ##print("FINDME R2: ", len(R))
        payload = pickle.dumps(ClientCompletePacket(picklable_R, S))
        length = (len(payload)).to_bytes(4, byteorder='little')
        self.out_buffer.append(packet_id+length+payload)
        #print("send_client_complete end")

    def send_client_ready(self, ready):
        #print("send_client_ready start")
        packet_id = client_server_packets["client_ready"].to_bytes(1, byteorder='little')
        payload = pickle.dumps(ClientReadyPacket(ready, self.configs_nodeids))
        #print("send_client_ready with configs:", self.configs_nodeids)
        length = len(payload).to_bytes(4, byteorder='little')
        out_packet = packet_id + length + payload
        self.out_buffer.append(out_packet)
        #print("send_client_ready end")

    def send_client_cpu_cores(self):
        #print("send_client_cpu_cores start")
        packet_id = client_server_packets["client_cpu_cores"].to_bytes(1, byteorder='little')
        cpu_cores = 28
        payload = pickle.dumps(ClientConfigPacket(cpu_cores))
        length = len(payload).to_bytes(4, byteorder='little')
        self.out_buffer.append(packet_id+length+payload)
        #print("send_client_cpu_cores end")
        
    def quit(self, message):
        print(message)
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()
        self.done = True

    def spin_wait_connection(self):
        try:
            address = "127.0.0.1"
            self.socket = socket.create_connection((address, 15025))
            packet_id = client_server_packets["client_hello"].to_bytes(1, byteorder='little')
            packet_pad = (0).to_bytes(4, byteorder='little')
            self.out_buffer.append(packet_id+packet_pad) #hello
            #print("Connected!")
            return True
        except:
            #print("Failed to connect...")
            time.sleep(1)
            return False
    def do_network_stuff(self):
        try:
            (read, write, err) = select.select([self.socket], [self.socket], [self.socket])
            for r in err:
                self.done = True
                quit("disconnected from error.")
                return
            for w in write:
                total_sent = 0
                while len(self.out_buffer) > 0:
                    #print(self.out_buffer)
                    b = self.out_buffer.pop()
                    sent = w.send(b)
                    if sent == 0:
                        quit("disconnected from send.")
                        self.done = True
                        self.connected = False
                    else:
                        total_sent += sent
            for r in read:
                data = []
                data = r.recv(9999)
                ##print("read data: ",data)
                if len(data) == 0 or data == b'':
                    self.done = True
                    quit("disconnected from read.")
                    self.connected = False
                else:
                    self.in_buffer.extend(data)
        except Exception as ex:
            #print("Something went wrong while doing socket stuff", ex)
            self.done = True
            self.connected = False
    def run(self):
        while not self.done:
            while not self.connected:
                self.connected = self.spin_wait_connection()
            self.do_network_stuff()
            self.received_callback()
            
                

if __name__ == "__main__":
    worker = Worker()
    worker.run()
    #print("Exited, for some reason?")