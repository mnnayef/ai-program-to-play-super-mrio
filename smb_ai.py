import torch
import torch.nn as nn
import sys

class Mario(nn.Module):
    def __init__(self, Ws, bs, bias=False):
        super(Mario, self).__init__()
        self.num_layers = len(Ws)
        self.relu = nn.ReLU()
        for i in range(len(Ws)):
            layer = nn.Linear(Ws[i].shape[0], Ws[i].shape[1], bias=bias).requires_grad_(False)
            layer.weight.copy_(Ws[i].T)
            if bias:
                layer.bias.copy_(bs[i])
            self.add_module("Linear"+str(i), layer)

    def forward(self,x):
        for name, _ in self.named_modules():
            if name != '':
                x = self._modules[name](x)
                x = self.relu(x)
        x = x.argmax(1)
        return x
    def get_layers(self):
        W = []
        b = []
        for name, layer in self.named_modules():
            if name.__contains__("Linear"): #skipping self
                W.append(layer.weight.T)
                b.append(layer.bias.T)
        #print("mario W: ", W)
        #print("mario b: ", b)
        #print("mario W[0] type: ", type(W[0]))
        #print("mario b[0] type: ", type(b[0]))
        return W, b