from PIL import Image, ImageOps
from pathlib import Path
import glob

#Fileformat: number of images (4 bytes) - [imagelength(4 bytes) - data(1B)*imagelength - classification 1B]
def saveTrainingData(data):
    number_of_images = len(data)
    if number_of_images < 1:
        return
    directory = Path('cnn_training')
    directory.mkdir(parents=True, exist_ok=True)
    file = Path('cnn_training/smb_cnn.data')
    file.touch(exist_ok=True)
    f = file.open('wb')
    f.write(number_of_images.to_bytes(4, byteorder='little', signed=True))
    for image in data:
        image_length = len(image[0])
        f.write(image_length.to_bytes(4, byteorder='little', signed=True))
        for pixel in image[0]:
            average = (pixel[0]+pixel[1]+pixel[2])//3
            f.write(average.to_bytes(1, byteorder='little', signed=False))
        f.write(image[1].to_bytes(1, byteorder='little', signed=False))
    f.close()

def readTrainingData():
    directory = Path('cnn_training')
    directory.mkdir(parents=True, exist_ok=True)
    file = Path('cnn_training/smb_cnn.data')
    file.touch(exist_ok=True)
    f = file.open('rb')
    #Read number of entries (once)
    number_of_images = int.from_bytes(f.read(4), byteorder='little', signed=True)
    x_train = []
    y_train = []
    #For each entry, read the greyscale value for each (x,y) pixel, and the classification
    for _ in range(number_of_images):
        image_length = int.from_bytes(f.read(4), byteorder='little', signed=True)
        image = []
        for _ in range(0, 16):
            row = []
            for _ in range(0, 16):
                row.append(int.from_bytes(f.read(1), byteorder='little', signed=False))
            image.append(row)
        x_train.append(image)
        y_train.append(int.from_bytes(f.read(1), byteorder='little', signed=False))
    return (x_train, y_train)

def getPixelData(im):
    im = im.convert('RGB')
    return list(im.getdata())
def generateMirror(image):
    mirroredImages = []
    cp = image.copy()
    mirroredImages.append(ImageOps.mirror(cp))
    return mirroredImages

def shift_x(image, x):
    """Shift an image horizontal."""
    return image.transform(image.size, Image.AFFINE, (1, 0, x, 0, 1, 0))

def shift_y(image, y):
    """Shift an image vertical."""
    return image.transform(image.size, Image.AFFINE, (1, 0, 0, 0, 1, y))

def generateShifts(image, shift=2):
    shiftedImages = []
    turn = [-shift, 0, shift]
    for straight in turn:
        for diag in turn:
            cp = image.copy()
            cp = shift_x(shift_y(cp, diag), straight)
            if cp.getextrema() != (0,0):
                shiftedImages.append(cp)
            else:
                print("all black image found")
    return shiftedImages

def merge(foreground, background_images):
    merged_images = []
    if(foreground.mode == "RGBA"):
        for bg_path in background_images:
            bg = Image.open(bg_path)
            fg_cp = foreground.copy()
            bg.paste(fg_cp, (0, 0), fg_cp)
            merged_images.append(bg)
    else: merged_images.append(foreground)
    return merged_images

def roll(im, delta, blocks):
    images = []
    for block in blocks:
        image = im.copy()
        xsize, ysize = image.size
        delta = delta % xsize
        if delta == 0: images.append(image)
        else:
            b = Image.open(block)
            part1 = image.crop((0, 0, delta, ysize))
            part2 = b.crop((delta, 0, xsize, ysize))
            image.paste(part2, (0, 0, xsize-delta, ysize))
            image.paste(part1, (xsize-delta, 0, xsize, ysize))
            images.append(image)
    return images

def generateTrainingData(generate):
    
    categories = []
    for path in glob.glob("cnn_training/data/*"):
        categories.append([f for f in glob.glob(path+"/*.png")])
        print(path.split("/")[len(path.split("/"))-1])

    if not generate:
        return len(categories)

    directory = Path('cnn_training')
    directory.mkdir(parents=True, exist_ok=True)
    file = Path('cnn_training/smb_cnn.data')
    file.touch(exist_ok=True)
    f = file.open('rb')
    number_of_images = int.from_bytes(f.read(4), byteorder='little', signed=True)

    all_images = []
    all_images_pixelated = []

    container = []
    bg_unique = []
    blocks = []

    for cat in categories:
        if("bgcolors" in cat[0]):
                cat.sort()
                print("bgcolors found for merging")
                bg_unique.append(cat[0])
                bg_unique.append(cat[16])
                bg_unique.append(cat[29])
                bg_unique.append(cat[36])
                bg_unique.append(cat[44])
                bg_unique.extend(cat[48:99])
        elif("blocks" in cat[0]):
            print("blocks found for rolling")
            for pic in cat:
                if("flag" not in pic and "spring" not in pic and "pipes" not in pic):
                    blocks.append(pic)

    for cat in categories:
        container.append([])

        for path in cat:
            image = Image.open(path)
            shifts = generateShifts(image)
            mirror = generateMirror(image)

            merged = []
            for img in mirror:
                merged.extend(merge(img, bg_unique))
            for img in shifts:
                merged.extend(merge(img, bg_unique))
            container[len(container)-1].extend(merged)

            if("blocks" in path and "flags" not in path and "springs" not in path and "pipes" not in path):
                rolled = roll(image, 10, blocks)
                container[len(container)-1].extend(rolled)

        all_images.append((container[len(container)-1], len(container)-1))

    for images in all_images:
        image_cat = images[1]
        print("Length of images in cat {}: {}".format(image_cat, len(images[0])))
        count = 0
        for image in images[0]:
            
            count += 1
            if(count % 10000 == 1):
                image.show(title="test "+str(count))
            
            all_images_pixelated.append((getPixelData(image), image_cat))
    saveTrainingData(all_images_pixelated)

    return len(categories)

def test():
    directory = Path('image_processing_test_folder')
    directory.mkdir(parents=True, exist_ok=True)
    image = Image.open("Untitled.png", 'r')
    variations = []
    variations.extend(generateRotations(image))
    variations.extend(generateShifts(image))
    i = 1
    for im in variations:
        im.save("image_processing_test_folder/"+str(i)+".png")
        i += 1

'''
im = Image.open('Untitled.png', 'r')
pix_val = list(im.getdata())
im2 = Image.open('1.png', 'r')
pix_val2 = list(im2.getdata())
im3 = Image.open('2.png', 'r')
pix_val3 = list(im3.getdata())

saveTrainingData([pix_val, pix_val2, pix_val3], 0)
x_train, y_train = readTrainingData("cnn_training/enemies.dat")
print("x_train\n", x_train, "\ny_train\n", y_train)
'''

'''
grey_scale = []
for pixel in pix_val:
    print(pixel)
    average = (pixel[0]+pixel[1]+pixel[2])//3
    grey_scale.append((average, average, average))

new_image = Image.new('RGB', (16,16))
new_image.putdata(grey_scale, 1, 0)
new_image.save('1.png', format='png')
'''

